#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <iconv/iconv.h>
#include <iconv-internal/iconv.h>

#include "testutils.h"

#ifdef __riscos__
#define ALIASES_FILE "Files.Aliases"
#else
#define ALIASES_FILE "Files/Aliases"
#endif

typedef struct nullable_testcase {
	const char *from_charset;
	const char *source;
	struct {
		size_t retval;
		int errval;
		size_t remaining;
	} expected;
} nullable_testcase;

static const nullable_testcase tests[] = {
	{ "iso-2022-jp", "1234\x1b$@123456\x1b(B", { 0, 0, 0 } },
	{ "iso-2022-jp", "1234\x1b$@123456\x1b(", { (size_t) -1, EINVAL, 2 } },
	{ NULL, NULL, { 0, 0, 0 } }
};

static void run_test(const nullable_testcase *test)
{
	iconv_t cd;
	char out[128];
	char *inp = (char *) test->source, *outp = out;
	size_t inlen = strlen(inp), outlen = sizeof(out);
	size_t read;

	cd = iconv_open("utf-8", test->from_charset);
	assert(cd != (iconv_t) -1);

	read = iconv(cd, &inp, &inlen, &outp, &outlen);
	assert(read == test->expected.retval);

	if (test->expected.retval == (size_t) -1) {
		assert(errno == test->expected.errval);
		assert(inlen == test->expected.remaining);
	}

	iconv_close(cd);
}

static void run_tests(void)
{
	int index;

	for (index = 0; tests[index].from_charset != NULL; index++) {
		run_test(&tests[index]);
	}
}

int main(int argc, char **argv)
{
	const char *ucpath;
	int alen;
	char aliases[4096];

	UNUSED(argc);
	UNUSED(argv);

#ifdef __riscos__
	ucpath = "Unicode:";
#else
	ucpath = getenv("UNICODE_DIR");
#endif

	assert(ucpath != NULL);

	strncpy(aliases, ucpath, sizeof(aliases) - 1);
	alen = strlen(aliases);
#ifndef __riscos__
	if (aliases[alen - 1] != '/') {
		strncat(aliases, "/", sizeof(aliases) - alen - 1);
		alen += 1;
	}
#endif
	strncat(aliases, ALIASES_FILE, sizeof(aliases) - alen - 1);
	aliases[sizeof(aliases) - 1] = '\0';

	assert(iconv_initialise(aliases) == 1);

	run_tests();

	iconv_finalise();

	printf("PASS\n");

	return 0;
}

