#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <iconv/iconv.h>
#include <iconv-internal/iconv.h>

#include "testutils.h"

#ifdef __riscos__
#define ALIASES_FILE "Files.Aliases"
#else
#define ALIASES_FILE "Files/Aliases"
#endif

typedef struct translit_testcase {
	const char *to_charset;
	const char *source;
	const char *expected;
} translit_testcase;

static const translit_testcase tests[] = {
	/* Trivial */
	{ "iso-8859-1//TRANSLIT", "\xe2\x80\x93", "-" },
	/* Multi-character replacements */
	{ "iso-8859-2//TRANSLIT", "\xc2\xa9", "(c)" },
	{ "iso-8859-3//TRANSLIT", "\xc2\xab", "<<" },
	/* Multiple choices */
	{ "iso-8859-4//TRANSLIT", "\xef\xac\x85", "st" },
	/* Default fallback */
	{ "iso-8859-1//TRANSLIT", "\xef\xac\x87", "?" },
	{ NULL, NULL, NULL }
};

static void run_test(const translit_testcase *test)
{
	iconv_t cd;
	char out[128];
	char *inp = (char *) test->source, *outp = out;
	size_t inlen = strlen(inp), outlen = sizeof(out);
	size_t read;

	cd = iconv_open(test->to_charset, "utf-8");
	assert(cd != (iconv_t) -1);

	read = iconv(cd, &inp, &inlen, &outp, &outlen);
	assert(read == 0);

	assert(sizeof(out) - outlen == strlen(test->expected));
	assert(memcmp(out, test->expected, sizeof(out) - outlen) == 0);

	iconv_close(cd);
}

static void run_tests(void)
{
	int index;

	for (index = 0; tests[index].to_charset != NULL; index++) {
		run_test(&tests[index]);
	}
}

static void test_translit_buffer_boundary(void)
{
	iconv_t cd;
	char out[128];
	char *inp = (char *) "\xc2\xa9", *outp = out;
	size_t inlen = strlen(inp), outlen;
	size_t read;

	cd = iconv_open("iso-8859-2//TRANSLIT", "utf-8");
	assert(cd != (iconv_t) -1);

	outlen = 1;
	read = iconv(cd, &inp, &inlen, &outp, &outlen);
	assert(read == (size_t) -1);
	assert(errno == E2BIG);

	/* Expect ( to appear in output */
	assert(outlen == 0);
	assert(out[0] == '(');

	/* Try to write next output character */
	outlen = 1;
	read = iconv(cd, &inp, &inlen, &outp, &outlen);
	assert(read == (size_t) -1);
	assert(errno == E2BIG);

	/* Expect "(c" in output */
	assert(outlen == 0);
	assert(out[0] == '(');
	assert(out[1] == 'c');

	/* Flush through last character */
	outlen = 1;
	read = iconv(cd, &inp, &inlen, &outp, &outlen);
	assert(read == 0);

	/* Expect "(c)" in output, and all input read */
	assert(outlen == 0);
	assert(memcmp(out, "(c)", 3) == 0);
	assert(inlen == 0);

	iconv_close(cd);
}

int main(int argc, char **argv)
{
	const char *ucpath;
	int alen;
	char aliases[4096];

	UNUSED(argc);
	UNUSED(argv);

#ifdef __riscos__
	ucpath = "Unicode:";
#else
	ucpath = getenv("UNICODE_DIR");
#endif

	assert(ucpath != NULL);

	strncpy(aliases, ucpath, sizeof(aliases) - 1);
	alen = strlen(aliases);
#ifndef __riscos__
	if (aliases[alen - 1] != '/') {
		strncat(aliases, "/", sizeof(aliases) - alen - 1);
		alen += 1;
	}
#endif
	strncat(aliases, ALIASES_FILE, sizeof(aliases) - alen - 1);
	aliases[sizeof(aliases) - 1] = '\0';

	assert(iconv_initialise(aliases) == 1);

	run_tests();
	test_translit_buffer_boundary();

	iconv_finalise();

	printf("PASS\n");

	return 0;
}

