# Component settings
COMPONENT := iconv
COMPONENT_VERSION := 0.13.0
# Default to a static library
COMPONENT_TYPE ?= lib-static

# Setup the tooling
PREFIX ?= /opt/netsurf
NSSHARED ?= $(PREFIX)/share/netsurf-buildsystem
include $(NSSHARED)/makefiles/Makefile.tools

TESTRUNNER := $(PERL) $(NSTESTTOOLS)/testrunner.pl

# Toolchain flags
DISABLED_WARNINGS := -Wno-format -Wno-missing-braces -Wno-sign-compare \
	-Wno-shift-negative-value -Wno-discarded-qualifiers \
	-Wno-unused-parameter
WARNFLAGS := -Wall -Wextra -Wundef -Wpointer-arith -Wcast-align \
	-Wwrite-strings -Wstrict-prototypes -Wmissing-prototypes \
	-Wmissing-declarations -Wnested-externs $(DISABLED_WARNINGS) \
	-Werror -pedantic
CFLAGS := -std=c99 -D_DEFAULT_SOURCE -I$(CURDIR)/include/ \
	-I$(CURDIR)/src $(WARNFLAGS) $(CFLAGS)

include $(NSBUILD)/Makefile.top

ifneq ($(HOST),arm-unknown-riscos)
  export UNICODE_DIR := $(BUILDDIR)/unires
endif

# Extra installation rules
I := /include/iconv
INSTALL_ITEMS := $(INSTALL_ITEMS) $(I):include/iconv/iconv.h
INSTALL_ITEMS := $(INSTALL_ITEMS) /lib/pkgconfig:lib$(COMPONENT).pc.in
INSTALL_ITEMS := $(INSTALL_ITEMS) /lib:$(OUTPUT)

ifeq ($(COMPONENT_TYPE),riscos-module)
  # And the RISC OS-specific targets

  DISTCLEAN_ITEMS := $(DISTCLEAN_ITEMS) iconv.zip iconv-pkg.zip

  .PHONY: riscos-dist

  # TODO: Make this sensible. Preferably by making use of the install target.
  riscos-dist: all $(BUILDDIR)/unires
	$(Q)$(CP) $(CPFLAGS) -r riscos riscos-dist
	$(Q)$(MKDIR) -p riscos-dist/!Boot/Resources
	$(Q)$(CP) $(CPFLAGS) -r $(BUILDDIR)/unires riscos-dist/!Boot/Resources/!Unicode
	$(Q)$(MKDIR) -p riscos-dist/!System/310/Modules
	$(Q)$(CP) $(CPFLAGS) $(BUILDDIR)/iconv,ffa riscos-dist/!System/310/Modules/Iconv,ffa
	$(Q)$(CP) $(CPFLAGS) COPYING riscos-dist
	$(Q)$(CP) $(CPFLAGS) -r doc riscos-dist
	$(Q)$(RM) $(RMFLAGS) -r riscos-dist/doc/Standards
	$(Q)$(CP) $(CPFLAGS) include/iconv/iconv.h riscos-dist/stubs/
	$(Q)$(CP) $(CPFLAGS) -r licenses riscos-dist
	$(Q)(cd riscos-dist ; $(GCCSDK_INSTALL_CROSSBIN)/zip -9r, ../iconv.zip *)
	$(Q)$(MV) $(MVFLAGS) riscos-dist/!Boot/Resources riscos-dist
	$(Q)$(RM) $(RMFLAGS) -r riscos-dist/!Boot
	$(Q)$(MV) $(MVFLAGS) riscos-dist/!System riscos-dist/System
	$(Q)$(RM) $(RMFLAGS) -r riscos-dist/doc riscos-dist/stubs 
	$(Q)$(RM) $(RMFLAGS) riscos-dist/ReadMe
	$(Q)$(CP) $(CPFLAGS) -r riscpkg/RiscPkg riscos-dist/
	$(Q)$(MV) $(MVFLAGS) riscos-dist/COPYING riscos-dist/RiscPkg/Copyright
	$(Q)$(RM) $(RMFLAGS) -r riscos-dist/licenses
	$(Q)(cd riscos-dist ; $(GCCSDK_INSTALL_CROSSBIN)/zip -9r, ../iconv-pkg.zip *)
	$(Q)$(RM) $(RMFLAGS) -r riscos-dist

endif

